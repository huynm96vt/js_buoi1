/**
 * Input:
 * -cd: 3cm
 * -cr: 2cm
 * 
 * (3+2)*2 = 10
 * 3*2 = 6
 * 
 * Output: 10, 6
 */

var cd = 3, cr = 2;
var cv = null, dt = null;

cv = (cd + cr)*2;
dt = cd*cr;

console.log("Chu vi la: " +cv);
console.log("Dien tich la: " +dt);