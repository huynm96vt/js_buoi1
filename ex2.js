/**
 * Input:
 * _input1 = 1.1;
 * _input2 = 2.2;
 * _input3 = 3.3;
 * _input4 = 4.4;
 * _input5 = 5.5;
 *
 * (1.1 + 2.2 + 3.3 + 4.4 + 5.5)/5;
 *
 * Output:
 */

var input1 = 1.1,
  input2 = 2.2,
  input3 = 3.3,
  input4 = 4.4,
  input5 = 5.5;

var output = null;

output = (input1 + input2 + input3 + input4 + input5) / 5;

console.log("Kết quả: " +output );
